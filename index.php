<?php
 
// Include Composer autoloader if not already done.
include 'vendor/autoload.php';
 
// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf    = $parser->parseFile('Noroxycdiff-Master-Label-páginas-1-7.pdf');
 
$text = $pdf->getText();

//echo $text;
$word = 'Interior hard';
$found = strpos($text, $word);
echo $found;

function delimitator($sample_text, $index, $characters){
    $line = substr($sample_text, $index, $characters);
    return $line;
}

echo $first_aid_title = delimitator($text, 1121, 9)."<br><br>";

echo $indications_1 = delimitator($text, 1133, 201)."<br><br>";

echo $if_in = "<li>".delimitator($text, 1336, 179)."</li>".
              "<li>".delimitator($text, 1517, 114)."</li>".
              "<li>".delimitator($text, 1631, 281)."</li>".
              "<li>".delimitator($text, 1913, 168)."</li>"."<br>";

echo $note_to_pshysician = delimitator($text, 2083, 90)."<br><br>";

echo $sporicidal = delimitator($text, 3383, 291)."<br>"."<br>".
                   "<li>".$clostridium_difficile = delimitator($text, 3675, 38)."</li>"."<br>";

$disinfection_indications_1 = delimitator($text, 3714, 457)."<br>"."<br>";
$disinfection_indications_2 = delimitator($text, 5107, 166)."<br>"."<br>";
$disinfection_indications_3 = delimitator($text, 6212, 166)."<br>"."<br>";

$disinfection_bacteria_list_1 = "<li>".$acinetobacter_baumannii = delimitator($text, 4172, 40)."</li>".
                                "<li>".$bordetella_pertussis = delimitator($text, 4213, 37)."</li>".
                                "<li>".$enterococcus_faecalis = delimitator($text, 4251, 68)."</li>".
                                "<li>".$escherichia_coli = delimitator($text, 4320, 32)."</li>".
                                "<li>".$escherichia_coli_extended = delimitator($text, 4352, 75)."</li>".
                                "<li>".$klebsiella_pneumoniae = delimitator($text, 4427, 37)."</li>".
                                "<li>".$klebsiella_pneumoniae_carbapenem_resistant = delimitator($text, 4463, 60)."</li>".
                                "<li>".$proteus_mirabilis = delimitator($text, 4524, 32)."</li>".
                                "<li>".$pseudomonas_aeruginosa = delimitator($text, 4558, 40)."</li>".
                                "<li>".$salmonella_enterica = delimitator($text, 4598, 37)."</li>".
                                "<li>".$staphylococcus_aureus = delimitator($text, 4635, 37)."</li>".
                                "<li>".$staphylococcus_aureus_usa300 = delimitator($text, 4670, 100)."</li>".
                                "<li>".$staphylococcus_aureus_usa400 = delimitator($text, 4771, 100)."</li>".
                                "<li>".$staphylococcus_aureus_33592 = delimitator($text, 4870, 73)."</li>".
                                "<li>".$staphylococcus_aureus_5836 = delimitator($text, 4943, 82)."</li>".
                                "<li>".$streptococcus_pneumoniae = delimitator($text, 5027, 39)."</li>".
                                "<li>".$streptococcus_pyogenes = delimitator($text, 5068, 38)."</li>"."<br>";

$disinfection_bacteria_list_2 = "<li>".$bordetella_bronchiseptica = delimitator($text, 6379, 42)."</li>".
                                "<li>".$campylobacter_jejuni = delimitator($text, 6422, 39)."</li>".
                                "<li>".$corynebacterium_ammoniagenes = delimitator($text, 6461, 44)."</li>".
                                "<li>".$enterococcus_faecalis = delimitator($text, 4251, 68)."</li>".
                                "<li>".$escherichia_coli_0157 = delimitator($text, 6574, 41)."</li>".
                                "<li>".$klebsiella_pneumoniae = delimitator($text, 4427, 37)."</li>".
                                "<li>".$listeria_monocytogenes = delimitator($text, 6653, 38)."</li>".
                                "<li>".$pseudomonas_aeruginosa = delimitator($text, 4558, 40)."</li>".
                                "<li>".$salmonella_enterica = delimitator($text, 4598, 37)."</li>".
                                "<li>".$salmonella_typhi = delimitator($text, 6767, 31)."</li>".
                                "<li>".$shigella_sonnei = delimitator($text, 6799, 31)."</li>".
                                "<li>".$staphylococcus_aureus = delimitator($text, 4635, 37)."</li>".
                                "<li>".$staphylococcus_aureus_usa400 = delimitator($text, 4771, 100)."</li>".
                                "<li>".$staphylococcus_aureus_5836 = delimitator($text, 4943, 82)."</li>";

echo $disinfection = $disinfection_indications_1.
                     $disinfection_bacteria_list_1.
                     $disinfection_indications_2.
                     $disinfection_bacteria_list_1.
                     $disinfection_indications_3.
                     $disinfection_bacteria_list_2."<br>";

echo $general_disifection = delimitator($text, 7050, 340)."<br>"."<br>".
                            "<li>".$bordetella_bronchiseptica = delimitator($text, 6379, 42)."</li>".
                            "<li>".$corynebacterium_ammoniagenes = delimitator($text, 6461, 44)."</li>".
                            "<li>".$enterococcus_faecalis = delimitator($text, 4251, 68)."</li>".
                            "<li>".$escherichia_coli_0157 = delimitator($text, 6574, 41)."</li>".
                            "<li>".$listeria_monocytogenes = delimitator($text, 6653, 38)."</li>".
                            "<li>".$salmonella_enterica = delimitator($text, 4598, 37)."</li>".
                            "<li>".$salmonella_typhi = delimitator($text, 6767, 31)."</li>".
                            "<li>".$shigella_sonnei = delimitator($text, 6799, 31)."</li>".
                            "<li>".$staphylococcus_aureus = delimitator($text, 4635, 37)."</li>".
                            "<li>".$staphylococcus_aureus_5836 = delimitator($text, 4943, 82)."</li>"."<br>";

$virucidal_indications_1 = delimitator($text, 7857, 186)."<br>"."<br>";
$virucidal_indications_2 = delimitator($text, 8753, 165)."<br>"."<br>";
$contact_time_indication = delimitator($text, 9446, 62)."<br>"."<br>";
$virucidal_indications_3 = delimitator($text, 9508, 166)."<br>"."<br>";
$virucidal_indications_4 = delimitator($text, 10008, 166)."<br>"."<br>";

$virucidal_bacteria_list_1 = "<li>".$herpes_simplex_virus_type_1 = delimitator($text, 8045, 44)."</li>".
                             "<li>".$herpes_simplex_virus_type_2 = delimitator($text, 8089, 46)."</li>".
                             "<li>".$human_immunodeficiency = delimitator($text, 8137, 75)."</li>".
                             "<li>".$influenza_a_virus = delimitator($text, 8214, 28)."</li>".
                             "<li>".$respiratory_syncytial_virus = delimitator($text, 8258, 47)."</li>".
                             "<li>".$rhinovirus_type_37 = delimitator($text, 8305, 30)."</li>".
                             "<li>".$rotavirus = delimitator($text, 8337, 24)."</li>".
                             "<li>".$vaccinia_virus = delimitator($text, 8362, 25)."</li>"."<br>";

$virucidal_bacteria_list_2 = "<li>".$adenovirus_type_5 = delimitator($text, 8919, 50)."</li>".
                             "<li>".$hepatitis_b_virus = delimitator($text, 8969, 31)."</li>".
                             "<li>".$hepatitis_c_virus = delimitator($text, 9001, 40)."</li>".
                             "<li>".$herpes_simplex_virus_type_1 = delimitator($text, 8045, 44)."</li>".
                             "<li>".$herpes_simplex_virus_type_2 = delimitator($text, 8089, 46)."</li>".
                             "<li>".$human_immunodeficiency = delimitator($text, 8137, 75)."</li>".
                             "<li>".$influenza_a_virus = delimitator($text, 8214, 28)."</li>".
                             "<li>".$norovirus = delimitator($text, 9244, 66)."</li>".
                             "<li>".$respiratory_syncytial_virus = delimitator($text, 8258, 47)."</li>".
                             "<li>".$rhinovirus_type_37 = delimitator($text, 8305, 30)."</li>".
                             "<li>".$rotavirus = delimitator($text, 8337, 24)."</li>".
                             "<li>".$vaccinia_virus = delimitator($text, 8362, 25)."</li>"."<br>";

$virucidal_bacteria_list_3 = "<li>".$avian_influenza_a = delimitator($text, 9675, 32)."</li>".
                             "<li>".$hepatitis_b_virus = delimitator($text, 8969, 31)."</li>".
                             "<li>".$herpes_simplex_virus_type_1 = delimitator($text, 8045, 44)."</li>".
                             "<li>".$herpes_simplex_virus_type_2 = delimitator($text, 8089, 46)."</li>".
                             "<li>".$human_immunodeficiency = delimitator($text, 8137, 75)."</li>".
                             "<li>".$human_coronavirus = delimitator($text, 9905, 30)."</li>".
                             "<li>".$infectious_bursal_disease = delimitator($text, 9935, 31)."</li>".
                             "<li>".$influenza_a_h1n1 = delimitator($text, 9966, 40)."</li>"."<br>";

$virucidal_bacteria_list_4 = "<li>".$avian_influenza_a = delimitator($text, 9675, 32)."</li>".
                             "<li>".$hepatitis_b_virus = delimitator($text, 8969, 31)."</li>".
                             "<li>".$herpes_simplex_virus_type_1 = delimitator($text, 8045, 44)."</li>".
                             "<li>".$herpes_simplex_virus_type_2 = delimitator($text, 8089, 46)."</li>".
                             "<li>".$human_immunodeficiency = delimitator($text, 8137, 75)."</li>".
                             "<li>".$human_coronavirus = delimitator($text, 9905, 30)."</li>".
                             "<li>".$infectious_bursal_disease = delimitator($text, 9935, 31)."</li>".
                             "<li>".$influenza_a_h1n1 = delimitator($text, 9966, 40)."</li>"."<br>";

echo $virucidal = $virucidal_indications_1.
                  $virucidal_bacteria_list_1.
                  $virucidal_indications_2.
                  $virucidal_bacteria_list_2.
                  $contact_time_indication.
                  $virucidal_indications_3.
                  $virucidal_bacteria_list_3.
                  $virucidal_indications_4.
                  $virucidal_bacteria_list_4;

$animal_premise_indications_1 = delimitator($text, 10507, 203)."<br>"."<br>";
$animal_premise_indications_2 = delimitator($text, 10851, 165)."<br>"."<br>";
$animal_premise_indications_3 = delimitator($text, 11449, 166)."<br>"."<br>";

$animal_premise_bacteria_list_1 = "<li>".$avian_influenza_a = delimitator($text, 10711, 43)."</li>".
                                  "<li>".$murine_norovirus = delimitator($text, 10756, 28)."</li>"."<br>";

$animal_premise_bacteria_list_2 = "<li>".$avian_adenovirus = delimitator($text, 11016, 34)."</li>".
                                  "<li>".$avian_adenovirus = delimitator($text, 11051, 60)."</li>".
                                  "<li>".$avian_influenza_a = delimitator($text, 9675, 32)."</li>".
                                  "<li>".$infectious_laryngotracheitis = delimitator($text, 11146, 55)."</li>".
                                  "<li>".$newcastle_disease_virus = delimitator($text, 11201, 35)."</li>".
                                  "<li>".$porcine_respiratory_virus = delimitator($text, 11237, 65)."</li>".
                                  "<li>".$porcine_rotavirus = delimitator($text, 11304, 29)."</li>".
                                  "<li>".$pseudorabies_virus = delimitator($text, 11334, 29)."</li>".
                                  "<li>".$transmissible_gastroenteritis_virus = delimitator($text, 11365, 43)."</li>".
                                  "<li>".$vesicular_stomatitis_virus = delimitator($text, 11410, 38)."</li>"."<br>";

$animal_premise_bacteria_list_3 = "<li>".$avian_adenovirus = delimitator($text, 11016, 34)."</li>".
                                  "<li>".$avian_adenovirus = delimitator($text, 11051, 60)."</li>".
                                  "<li>".$avian_influenza_a = delimitator($text, 9675, 32)."</li>".
                                  "<li>".$infectious_laryngotracheitis = delimitator($text, 11146, 55)."</li>".
                                  "<li>".$newcastle_disease_virus = delimitator($text, 11201, 35)."</li>".
                                  "<li>".$porcine_respiratory_virus = delimitator($text, 11237, 65)."</li>".
                                  "<li>".$porcine_rotavirus = delimitator($text, 11304, 29)."</li>".
                                  "<li>".$pseudorabies_virus = delimitator($text, 11334, 29)."</li>".
                                  "<li>".$transmissible_gastroenteritis_virus = delimitator($text, 11365, 43)."</li>".
                                  "<li>".$vesicular_stomatitis_virus = delimitator($text, 11410, 38)."</li>"."<br>";

echo $animal_premise = $animal_premise_indications_1.
                       $animal_premise_bacteria_list_1.
                       $contact_time_indication.
                       $animal_premise_indications_2.
                       $animal_premise_bacteria_list_2.
                       $animal_premise_indications_3.
                       $animal_premise_bacteria_list_3;

$food_contact_indications_1 = delimitator($text, 12043, 198)."<br>"."<br>";
$food_contact_indications_2 = delimitator($text, 13542, 214)."<br>"."<br>";

$food_contact_bacteria_list_1 = "<li>".$aeromonas_hydrophila = delimitator($text, 12242, 36)."</li>".
                                "<li>".$clostridium_perfringens = delimitator($text, 12280, 52)."</li>".
                                "<li>".$enterobacter_sakazakii = delimitator($text, 12333, 38)."</li>".
                                "<li>".$escherichia_coli = delimitator($text, 4320, 32)."</li>".
                                "<li>".$escherichia_coli_o26 = delimitator($text, 12405, 40)."</li>".
                                "<li>".$escherichia_coli_o45 = delimitator($text, 12445, 41)."</li>".
                                "<li>".$escherichia_coli_o103 = delimitator($text, 12486, 45)."</li>".
                                "<li>".$escherichia_coli_o111 = delimitator($text, 12531, 40)."</li>".
                                "<li>".$escherichia_coli_o121 = delimitator($text, 12571, 42)."</li>".
                                "<li>".$escherichia_coli_o145 = delimitator($text, 12613, 42)."</li>".
                                "<li>".$escherichia_coli_o157 = delimitator($text, 12655, 42)."</li>".
                                "<li>".$klebsiella_pneumoniae = delimitator($text, 4427, 37)."</li>".
                                "<li>".$salmonella_enterica = delimitator($text, 4598, 37)."</li>".
                                "<li>".$salmonella_enterica_serotype = delimitator($text, 12772, 57)."</li>".
                                "<li>".$salmonella_typhi = delimitator($text, 6767, 31)."</li>".
                                "<li>".$shigella_dysenteriae = delimitator($text, 12864, 36)."</li>".
                                "<li>".$shigella_sonnei = delimitator($text, 6799, 31)."</li>".
                                "<li>".$staphylococcus_aureus = delimitator($text, 4635, 37)."</li>".
                                "<li>".$xanthomonas_axonopodis = delimitator($text, 12974, 56)."</li>".
                                "<li>".$yersinia_enterocolitica = delimitator($text, 13031, 40)."</li>"."<br>";

$food_contact_bacteria_list_2 = "<li>".$aeromonas_hydrophila = delimitator($text, 12242, 36)."</li>".
                                "<li>".$clostridium_perfringens = delimitator($text, 12280, 52)."</li>".
                                "<li>".$enterobacter_sakazakii = delimitator($text, 12333, 38)."</li>".
                                "<li>".$escherichia_coli = delimitator($text, 4320, 32)."</li>".
                                "<li>".$escherichia_coli_o26 = delimitator($text, 12405, 40)."</li>".
                                "<li>".$escherichia_coli_o45 = delimitator($text, 12445, 41)."</li>".
                                "<li>".$escherichia_coli_o103 = delimitator($text, 12486, 45)."</li>".
                                "<li>".$escherichia_coli_o111 = delimitator($text, 12531, 40)."</li>".
                                "<li>".$escherichia_coli_o121 = delimitator($text, 12571, 42)."</li>".
                                "<li>".$escherichia_coli_o145 = delimitator($text, 12613, 42)."</li>".
                                "<li>".$escherichia_coli_o157 = delimitator($text, 12655, 42)."</li>".
                                "<li>".$klebsiella_pneumoniae = delimitator($text, 4427, 37)."</li>".
                                "<li>".$salmonella_enterica = delimitator($text, 4598, 37)."</li>".
                                "<li>".$salmonella_enterica_serotype = delimitator($text, 12772, 57)."</li>".
                                "<li>".$salmonella_typhi = delimitator($text, 6767, 31)."</li>".
                                "<li>".$shigella_dysenteriae = delimitator($text, 12864, 36)."</li>".
                                "<li>".$shigella_sonnei = delimitator($text, 6799, 31)."</li>".
                                "<li>".$yersinia_enterocolitica = delimitator($text, 13031, 40)."</li>"."<br>";

echo $food_contact = $food_contact_indications_1.
                     $food_contact_bacteria_list_1.
                     $food_contact_indications_2.
                     $food_contact_bacteria_list_2;

$fungicidal_indications_1 = delimitator($text, 14477, 198)."<br>"."<br>";
$fungicidal_indications_2 = delimitator($text, 14711, 180)."<br>"."<br>";
$fungicidal_indications_3 = delimitator($text, 15020, 183)."<br>"."<br>";
$fungicidal_indications_4 = delimitator($text, 15301, 183)."<br>"."<br>";

$fungicidal_bacteria_list_1 = "<li>".$candida_albicans = delimitator($text, 14678, 33)."</li>"."<br>";

$fungicidal_bacteria_list_2 = "<li>".$candida_albicans = delimitator($text, 14678, 33)."</li>".
                              "<li>".$trichophyton_mentagrophytes = delimitator($text, 14925, 95)."</li>"."<br>";

$fungicidal_bacteria_list_3 = "<li>".$trichophyton_mentagrophytes = delimitator($text, 14925, 95)."</li>"."<br>";

$fungicidal_bacteria_list_4 = "<li>".$trichophyton_mentagrophytes = delimitator($text, 14925, 95)."</li>"."<br>";

echo $fungicidal = $fungicidal_indications_1.
                   $fungicidal_bacteria_list_1.
                   $fungicidal_indications_2.
                   $fungicidal_bacteria_list_2.
                   $fungicidal_indications_3.
                   $fungicidal_bacteria_list_3.
                   $fungicidal_indications_4.
                   $fungicidal_bacteria_list_4;


echo $locations = delimitator($text, 19917, 18)."<br>"."<br>".
                  "<li>".$airline_terminals = delimitator($text, 20636, 154)."</li>".
                  "<li>".$automobiles = delimitator($text, 20797, 92)."</li>".
                  "<li>".$boats = delimitator($text, 20894, 61)."</li>".
                  "<li>".$boxcars = delimitator($text, 20962, 35)."</li>".
                  "<li>".$buses = delimitator($text, 21002, 69)."</li>".
                  "<li>".$delivery_trucks = delimitator($text, 21076, 55)."</li>".
                  "<li>".$ems = delimitator($text, 21136, 79)."</li>".
                  "<li>".$police_stations = delimitator($text, 21222, 161)."</li>".
                  "<li>".$recycling_centers = delimitator($text, 21389, 17)."</li>".
                  "<li>".$athletic_facilities = delimitator($text, 21413, 104)."</li>".
                  "<li>".$banks = delimitator($text, 21522, 42)."</li>".
                  "<li>".$campgrounds = delimitator($text, 21569, 68)."</li>".
                  "<li>".$day_care_centers = delimitator($text, 21644, 69)."</li>".
                  "<li>".$funeral_homes = delimitator($text, 21720, 88)."</li>".
                  "<li>".$hotels = delimitator($text, 21813, 17)."</li>".
                  "<li>".$museums = delimitator($text, 21834, 82)."</li>".
                  "<li>".$restaurants = delimitator($text, 21922, 138)."</li>".
                  "<li>".$schools = delimitator($text, 22065, 78)."</li>".
                  "<li>".$sports_arenas = delimitator($text, 22147, 34)."</li>".
                  "<li>".$supermarkets = delimitator($text, 22185, 195)."</li>".
                  "<li>".$veterinary = delimitator($text, 22386, 497)."</li>".
                  "<li>".$businesses = delimitator($text, 22888, 107)."</li>".
                  "<li>".$commercial = delimitator($text, 23000, 157)."</li>".
                  "<li>".$cosmetic_manufacturing = delimitator($text, 23161, 140)."</li>".
                  "<li>".$factories = delimitator($text, 23304, 70)."</li>".
                  "<li>".$institutional = delimitator($text, 23378, 130)."</li>".
                  "<li>".$laboratories = delimitator($text, 23514, 15)."</li>".
                  "<li>".$basements = delimitator($text, 23533, 75)."</li>".
                  "<li>".$bathrooms = delimitator($text, 23612, 60)."</li>".
                  "<li>".$homes = delimitator($text, 23676, 84)."</li>".
                  "<li>".$kitchens = delimitator($text, 23767, 48)."</li>".
                  "<li>".$dairy = delimitator($text, 23818, 38)."</li>".
                  "<li>".$farmhouses = delimitator($text, 23861, 280)."</li>".
                  "<li>".$federally = delimitator($text, 24147, 45)."</li>".
                  "<li>".$food_establishments = delimitator($text, 24197, 100)."</li>".
                  "<li>".$food_handling = delimitator($text, 24304, 31)."</li>".
                  "<li>".$food_processing_plants = delimitator($text, 24693, 350)."</li>".
                  "<li>".$poultry_premise = delimitator($text, 25048, 31)."</li>".
                  "<li>".$processing_facilities = delimitator($text, 25256, 110)."</li>".
                  "<li>".$tobacco = delimitator($text, 25535, 25)."</li>".
                  "<li>".$hospitals = delimitator($text, 25562, 364)."</li>".
                  "<li>".$life_care = delimitator($text, 25932, 76)."</li>".
                  "<li>".$patient_care = delimitator($text, 26014, 324)."</li>"."<br>";

echo $material_comatibility = delimitator($text, 26352, 22)."<br>"."<br>".
                              $not_recomended =  delimitator($text, 26377, 146)."<br>"."<br>".
                              $note = delimitator($text, 26524, 245)."<br>"."<br>".
                              "<li>".$glass_surfaces = delimitator($text, 26773, 527)."</li>".
                              "<li>".$countertops = delimitator($text, 27306, 191)."</li>".
                              "<li>".$dishes_glassware = delimitator($text, 27503, 280)."</li>".
                              "<li>".$floors = delimitator($text, 27791, 100)."</li>".
                              "<li>".$highchairs = delimitator($text, 27898, 170)."</li>".
                              "<li>".$shower_stalls = delimitator($text, 28073, 178)."</li>".
                              "<li>".$tables = delimitator($text, 28257, 300)."</li>".
                              "<li>".$sealed_foundations = delimitator($text, 28562, 81)."</li>".
                              "<li>".$and_other = delimitator($text, 28650, 38)."</li>".
                              "<li>".$automobile_interiors = delimitator($text, 28692, 55)."</li>".
                              "<li>".$commercial_florist = delimitator($text, 28751, 75)."</li>".
                              "<li>".$crypton = delimitator($text, 28831, 25)."</li>".
                              "<li>".$hard_hats = delimitator($text, 28859, 23)."</li>".
                              "<li>".$hard_non_porous = delimitator($text, 28887, 65)."</li>".
                              "<li>".$kennel_runs = delimitator($text, 28958, 145)."</li>".
                              "<li>".$large_inflatable = delimitator($text, 29108, 163)."</li>".
                              "<li>".$maintenance_equipment = delimitator($text, 29275, 24)."</li>".
                              "<li>".$playground_equipment = delimitator($text, 29302, 22)."</li>".
                              "<li>".$non_wooden = delimitator($text, 29329, 80)."</li>".
                              "<li>".$drinking_fountains = delimitator($text, 29413, 20)."</li>".
                              "<li>".$telephones = delimitator($text, 29438, 32)."</li>".
                              "<li>".$wrestling = delimitator($text, 29476, 210)."</li>".
                              "<li>".$beer_fermentation = delimitator($text, 30042, 80)."</li>".
                              "<li>".$citrus_processing = delimitator($text, 30126, 47)."</li>".
                              "<li>".$equipment_pipelines = delimitator($text, 30178, 220)."</li>".
                              "<li>".$food_preparation = delimitator($text, 30403, 42)."</li>".
                              "<li>".$hatchers = delimitator($text, 30450, 240)."</li>".
                              "<li>".$harvesting = delimitator($text, 30694, 32)."</li>".
                              "<li>".$ice_machines = delimitator($text, 30730, 13)."</li>".
                              "<li>".$kitchen_equipment = delimitator($text, 30748, 100)."</li>".
                              "<li>".$meat_packing = delimitator($text, 30852, 503)."</li>".
                              "<li>".$wine_processing = delimitator($text, 31361, 45)."</li>".
                              "<li>".$tobacco_equipment = delimitator($text, 31410, 25)."</li>".
                              "<li>".$hospital_beds = delimitator($text, 31440, 480)."</li>".
                              "<li>".$external_lenses = delimitator($text, 31926, 180)."</li>".
                              "<li>".$exhaust_fans = delimitator($text, 32111, 140)."</li>".
                              "<li>".$interior_surfaces = delimitator($text, 32258, 148)."</li>";


?>